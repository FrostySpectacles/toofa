# Toofa

Toofa is a CLI-based 2FA credential manager.

---

**⚠️ This is pre-release software. Breaking changes to the credential store format may occur without offering a migration path.**

---

- Fast access to your tokens, thanks to our patent-pending absence of Electron
- Accounts are stored locally with AES-256-GCM encryption
- Easy to backup (just copy the config file somewhere safe)
- Supports bulk import and export of accounts
- Integrates with your system clipboard
- Primarily tested on Windows, seems to work on Linux, may work on macOS


## Usage
To see all available commands, just run Toofa without parameters.


### Initiating your storage
![Demo](docs/video/init.mp4)


### Adding accounts
You can do this in several ways:
- Put a screenshot of one or more 2FA QR codes on your system clipboard and run `toofa paste`
- Copy a newline-separated list of `otpauth://` links on your system clipboard and run `toofa paste`
- In environments where clipboard is not supported, run `toofa add` and input an `otpauth://` URL when prompted

![Demo](docs/video/add.mp4)


### Generating tokens
![Demo](docs/video/generate.mp4)


### Multi-device usage
If you use Toofa on multiple devices, your credential stores may diverge over time. Given a credential store from another machine, `toofa merge` will:

- Import new credentials you don't have yet
- Update credentials that got edited
- Delete credentials you removed on your other machine

![Demo](docs/video/merge.mp4)
