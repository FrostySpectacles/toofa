use aes_gcm::aead::consts::U12;
use aes_gcm::aead::generic_array::GenericArray;
use aes_gcm::aead::Aead;
use aes_gcm::aes::Aes256;
use aes_gcm::Aes256Gcm;
use aes_gcm::AesGcm;
use aes_gcm::KeyInit;
use argon2::{password_hash::rand_core::OsRng, Argon2};

use chrono::DateTime;
use chrono::Utc;
use rand::RngCore;
use serde::{Deserialize, Serialize};

use crate::{
    io::BufReader,
    tokens::{Credential, CredentialStore},
};
use core::fmt;
use std::fs::create_dir_all;
use std::io::BufRead;
use std::io::Cursor;
use std::io::Write;

use std::{
    collections::HashSet,
    fs::{self, File},
    io::Read,
    path::Path,
};

const ENCRYPTION_FMT_0_HEADER: &str = "-- TOOFA ENCRYPT FMT0 --";

#[derive(Serialize, Deserialize)]
struct Config {
    pub accounts: Vec<Credential>,
    pub deleted_account_ids: HashSet<String>,
    #[serde(with = "chrono::serde::ts_nanoseconds")]
    pub last_modified: DateTime<Utc>,
}

#[derive(Debug, PartialEq)]
pub enum LoadError {
    IoError,
    ParseError,
    DuplicateId,
    DecryptionError,
}

#[derive(Debug)]
pub enum SaveError {
    ExportError,
    WriteError,
    EncryptionError,
}

pub fn sniff_config_file_type(path: &Path) -> Result<ConfigFileType, LoadError> {
    let file = File::open(path).map_err(|_| LoadError::IoError)?;
    let mut buf_reader = BufReader::new(file);

    let mut buffer = String::new();
    buf_reader
        .read_line(&mut buffer)
        .map_err(|_| LoadError::IoError)?;

    match buffer.trim_end() {
        ENCRYPTION_FMT_0_HEADER => Ok(ConfigFileType::Encrypted(EncryptionVersion::V0)),
        _ => Ok(ConfigFileType::Unencrypted),
    }
}

fn load_file_data_into_cred_store<R>(
    reader: BufReader<R>,
    encryption_key: Option<EncryptionKey>,
) -> Result<CredentialStore, LoadError>
where
    R: Read,
{
    let config: Config = serde_json::from_reader(reader).map_err(|_| LoadError::ParseError)?;

    let all_ids: Vec<String> = config
        .accounts
        .iter()
        .map(|credential| credential.id.clone())
        .collect();
    let unique_ids: HashSet<&String> = all_ids.iter().collect();
    if unique_ids.len() != all_ids.len() {
        return Err(LoadError::DuplicateId);
    }

    let cred_store = CredentialStore::load(
        encryption_key,
        config.accounts,
        config.deleted_account_ids,
        config.last_modified,
    );
    Ok(cred_store)
}

fn export_cred_store_to_str(cred_store: &CredentialStore) -> Result<String, ()> {
    let config = Config {
        accounts: cred_store.credentials.clone(),
        deleted_account_ids: cred_store.deleted_credential_ids.clone(),
        last_modified: cred_store.last_modified,
    };
    serde_json::to_string(&config).map_err(|_| ())
}

#[derive(Debug)]
pub enum EncryptionVersion {
    V0,
}

#[derive(Debug)]
pub enum ConfigFileType {
    Unencrypted,
    Encrypted(EncryptionVersion),
}

fn get_header_for_encryption_version(encryption_version: &EncryptionVersion) -> &str {
    match encryption_version {
        EncryptionVersion::V0 => ENCRYPTION_FMT_0_HEADER,
    }
}

pub fn get_encrypted_cred_store_from_config(
    path: &Path,
    password: &str,
    encryption_version: &EncryptionVersion,
) -> Result<CredentialStore, LoadError> {
    let file = File::open(path).map_err(|_| LoadError::IoError)?;
    let mut reader = BufReader::new(file);

    let mut actual_header_line = String::new();
    let expected_header_line = get_header_for_encryption_version(encryption_version);
    reader
        .read_line(&mut actual_header_line)
        .expect("Unable to read header line");
    if expected_header_line != actual_header_line.trim_end() {
        panic!(
            "Expected header line {} should match actual header line {}",
            expected_header_line,
            actual_header_line.trim_end()
        );
    }

    let mut derivation_salt = [0u8; 32];
    reader
        .read_exact(&mut derivation_salt)
        .expect("Could not read key derivation salt");

    let encryption_key = EncryptionKey::from_password(password, derivation_salt);

    let mut nonce_array = [0u8; 12];
    reader
        .read_exact(&mut nonce_array)
        .expect("Unable to read nonce");
    let nonce = GenericArray::from_slice(&nonce_array);

    let mut ciphertext = Vec::new();
    reader
        .read_to_end(&mut ciphertext)
        .expect("Unable to read ciphertext");

    let decoded_text_bytes = encryption_key
        .cipher
        .decrypt(nonce, ciphertext.as_ref())
        .map_err(|_| LoadError::DecryptionError)?;

    let cursor = Cursor::new(decoded_text_bytes);
    let reader = BufReader::new(cursor);

    let cred_store = load_file_data_into_cred_store(reader, Some(encryption_key))?;
    Ok(cred_store)
}

pub fn get_unencrypted_cred_store_from_config(path: &Path) -> Result<CredentialStore, LoadError> {
    let file = File::open(path).map_err(|_| LoadError::IoError)?;
    let reader = BufReader::new(file);
    let cred_store = load_file_data_into_cred_store(reader, None)?;
    Ok(cred_store)
}

pub fn save_cred_store_to_config(
    path: &Path,
    cred_store: &CredentialStore,
) -> Result<(), SaveError> {
    let file_contents = export_cred_store_to_str(cred_store).map_err(|_| SaveError::ExportError)?;
    create_dir_all(path.parent().unwrap()).map_err(|_| SaveError::WriteError)?;

    match &cred_store.encryption_key {
        None => {
            fs::write(path, &file_contents).map_err(|_| SaveError::WriteError)?;
            Ok(())
        }
        Some(encryption_key) => {
            let mut nonce_array = [0u8; 12];
            OsRng.fill_bytes(&mut nonce_array);
            let nonce = GenericArray::from_slice(&nonce_array);

            let ciphertext = encryption_key
                .cipher
                .encrypt(nonce, file_contents.as_bytes().as_ref())
                .map_err(|_| SaveError::EncryptionError)?;

            let mut buffer = File::create(path).map_err(|_| SaveError::WriteError)?;
            buffer
                .write_all(format!("{}\n", ENCRYPTION_FMT_0_HEADER).as_bytes())
                .map_err(|_| SaveError::WriteError)?;
            buffer
                .write_all(&encryption_key.derivation_salt)
                .map_err(|_| SaveError::WriteError)?;
            buffer.write_all(nonce).map_err(|_| SaveError::WriteError)?;
            buffer
                .write_all(&ciphertext)
                .map_err(|_| SaveError::WriteError)?;

            Ok(())
        }
    }
}

#[derive(Clone)]
pub struct EncryptionKey {
    derivation_salt: [u8; 32],
    cipher: AesGcm<Aes256, U12>,
}

impl fmt::Debug for EncryptionKey {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("EncryptionKey").finish()
    }
}

impl EncryptionKey {
    pub fn from_password(password: &str, derivation_salt: [u8; 32]) -> Self {
        let argon2 = Argon2::default();
        let mut derived_password = [0u8; 32];
        argon2
            .hash_password_into(password.as_bytes(), &derivation_salt, &mut derived_password)
            .expect("Key derivation failed");

        EncryptionKey {
            derivation_salt,
            cipher: Aes256Gcm::new_from_slice(&derived_password)
                .expect("Encryption key generation failed"),
        }
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;
    use std::{collections::HashSet, fs::remove_file, io::BufReader, path::Path};

    use aes_gcm::aead::OsRng;
    use chrono::{DateTime, Utc};
    use rand::RngCore;
    use serde_json::{json, Value};
    use totp_rs::TOTP;

    use crate::{
        file_ops::LoadError,
        tokens::{Credential, CredentialStore},
    };

    use super::{
        export_cred_store_to_str, get_encrypted_cred_store_from_config,
        load_file_data_into_cred_store, save_cred_store_to_config, EncryptionKey,
        EncryptionVersion,
    };

    #[test]
    fn test_parse_empty_reader_data() {
        let json_str = serde_json::to_string(&json!({
            "accounts": [],
            "deleted_account_ids": [],
            "last_modified": 946684800000000000 as i64
        }))
        .unwrap();
        let reader = BufReader::new(json_str.as_bytes());
        let cred_store = load_file_data_into_cred_store(reader, None).unwrap();

        assert_eq!(
            cred_store.last_modified,
            DateTime::parse_from_rfc3339("2000-01-01T00:00:00Z").unwrap()
        );
        assert_eq!(cred_store.deleted_credential_ids, HashSet::<String>::new());
        assert_eq!(cred_store.search_by_name(""), []);
    }

    #[test]
    fn test_parse_deduplicate_deleted_account_ids() {
        let json_str = serde_json::to_string(&json!({
            "accounts": [],
            "deleted_account_ids": ["1234", "1234"],
            "last_modified": 946684800000000000 as i64
        }))
        .unwrap();
        let reader = BufReader::new(json_str.as_bytes());
        let cred_store = load_file_data_into_cred_store(reader, None).unwrap();

        assert_eq!(
            cred_store.last_modified,
            DateTime::parse_from_rfc3339("2000-01-01T00:00:00Z").unwrap()
        );
        assert_eq!(
            cred_store.deleted_credential_ids,
            HashSet::from([String::from("1234")])
        );
        assert_eq!(cred_store.search_by_name(""), []);
    }

    #[test]
    fn test_parse_last_modified() {
        let json_str = serde_json::to_string(&json!({
            "accounts": [
                {
                    "id": "1234",
                    "totp_url": "otpauth://totp/Foo?secret=xy6uz4tcs3mk5bxemykmtatvd7i7vndcekgpb2j2ushbko23bvashifp&digits=7&period=10",
                    "created": 1672603283000000000 as i64,
                    "last_modified": 1679305017000000000 as i64
                }
            ],
            "deleted_account_ids": ["5678"],
            "last_modified": 946684800000000000 as i64
        })).unwrap();
        let reader = BufReader::new(json_str.as_bytes());
        let cred_store = load_file_data_into_cred_store(reader, None).unwrap();

        assert_eq!(
            cred_store.deleted_credential_ids,
            HashSet::from([String::from("5678")])
        );
        assert_eq!(
            cred_store.last_modified,
            DateTime::parse_from_rfc3339("2000-01-01T00:00:00Z").unwrap()
        );
    }

    #[test]
    fn test_parse_one_credential() {
        let json_str = serde_json::to_string(&json!({
            "accounts": [
                {
                    "id": "1234",
                    "totp_url": "otpauth://totp/Foo?secret=xy6uz4tcs3mk5bxemykmtatvd7i7vndcekgpb2j2ushbko23bvashifp&digits=7&period=10",
                    "created": 1672603283000000000 as i64,
                    "last_modified": 1679305017000000000 as i64
                }
            ],
            "deleted_account_ids": ["5678"],
            "last_modified": 946684800000000000 as i64
        })).unwrap();
        let reader = BufReader::new(json_str.as_bytes());
        let cred_store = load_file_data_into_cred_store(reader, None).unwrap();

        assert_eq!(
            cred_store.deleted_credential_ids,
            HashSet::from([String::from("5678")])
        );
        assert_eq!(cred_store.search_by_name(""), [Credential {
            id: "1234".to_string(),
            totp: TOTP::from_url("otpauth://totp/Foo?secret=xy6uz4tcs3mk5bxemykmtatvd7i7vndcekgpb2j2ushbko23bvashifp&digits=7&period=10").unwrap(),
            created: DateTime::parse_from_rfc3339("2023-01-01T20:01:23Z").unwrap().with_timezone(&Utc),
            last_modified: DateTime::parse_from_rfc3339("2023-03-20T09:36:57Z").unwrap().with_timezone(&Utc),
        }]);
    }

    #[test]
    fn test_block_duplicate_ids() {
        let json_str = serde_json::to_string(&json!({
            "accounts": [
                {
                    "id": "1234",
                    "totp_url": "otpauth://totp/Foo?secret=xy6uz4tcs3mk5bxemykmtatvd7i7vndcekgpb2j2ushbko23bvashifp&digits=7&period=10",
                    "created": 1672603283000000000 as i64,
                    "last_modified": 1679305017000000000 as i64
                },
                {
                    "id": "1234",
                    "totp_url": "otpauth://totp/Foo?secret=xy6uz4tcs3mk5bxemykmtatvd7i7vndcekgpb2j2ushbko23bvashifp&digits=7&period=10",
                    "created": 1672603286000000000 as i64,
                    "last_modified": 1672603286000000000 as i64
                }
            ],
            "deleted_account_ids": [],
            "last_modified": 946684800000000000 as i64
        })).unwrap();
        let reader = BufReader::new(json_str.as_bytes());
        let cred_store = load_file_data_into_cred_store(reader, None);
        assert_eq!(cred_store.unwrap_err(), LoadError::DuplicateId);
    }

    #[test]
    fn test_export_one_credential() {
        let mut cred_store = CredentialStore::new(None);
        cred_store.add_from_url("otpauth://totp/Foo?secret=XY6UZ4TCS3MK5BXEMYKMTATVD7I7VNDCEKGPB2J2USHBKO23BVASHIFP&digits=7&period=10").unwrap();

        let export_str = export_cred_store_to_str(&cred_store).unwrap();
        let export_json: Value = serde_json::from_str(&export_str).unwrap();
        assert_eq!(
            json!(export_json),
            json!({
                "accounts": [
                    {
                        "id": cred_store.credentials[0].id,
                        "totp_url": "otpauth://totp/Foo?secret=XY6UZ4TCS3MK5BXEMYKMTATVD7I7VNDCEKGPB2J2USHBKO23BVASHIFP&digits=7&period=10",
                        "created": cred_store.credentials[0].created.timestamp_nanos(),
                        "last_modified": cred_store.credentials[0].last_modified.timestamp_nanos(),
                    }
                ],
                "deleted_account_ids": [],
                "last_modified": cred_store.credentials[0].last_modified.timestamp_nanos(),
            })
        );
    }

    fn _remove_if_present(path: &Path) {
        if path.exists() {
            remove_file(path).unwrap();
        }
    }

    #[test]
    fn test_encrypt_and_decrypt_fmt0() {
        let test_path = Path::new("_test_file_encrypt_decrypt.json");
        _remove_if_present(test_path);

        let mut derivation_salt = [0u8; 32];
        OsRng.fill_bytes(&mut derivation_salt);

        let encryption_key = EncryptionKey::from_password("testpassword", derivation_salt);
        let mut cred_store = CredentialStore::new(Some(encryption_key));
        let otp_url = "otpauth://totp/Foo?secret=XY6UZ4TCS3MK5BXEMYKMTATVD7I7VNDCEKGPB2J2USHBKO23BVASHIFP&digits=7&period=10";
        cred_store.add_from_url(otp_url).unwrap();
        let cred_before_save = cred_store.search_by_name("")[0].clone();

        save_cred_store_to_config(test_path, &cred_store).unwrap();
        let loaded_cred_store =
            get_encrypted_cred_store_from_config(test_path, "testpassword", &EncryptionVersion::V0)
                .unwrap();

        let results = loaded_cred_store.search_by_name("");
        assert_eq!(results.len(), 1);
        assert_eq!(results[0], cred_before_save);
        _remove_if_present(test_path);
    }

    #[test]
    fn test_mismatching_password_fails_fmt0() {
        let test_path = Path::new("_test_file_mismatch_password.json");
        _remove_if_present(test_path);

        let mut derivation_salt = [0u8; 32];
        OsRng.fill_bytes(&mut derivation_salt);

        let encryption_key = EncryptionKey::from_password("testpassword", derivation_salt);
        let mut cred_store = CredentialStore::new(Some(encryption_key));
        let otp_url = "otpauth://totp/Foo?secret=XY6UZ4TCS3MK5BXEMYKMTATVD7I7VNDCEKGPB2J2USHBKO23BVASHIFP&digits=7&period=10";
        cred_store.add_from_url(otp_url).unwrap();

        save_cred_store_to_config(test_path, &cred_store).unwrap();
        let error = get_encrypted_cred_store_from_config(
            test_path,
            "otherpassword",
            &EncryptionVersion::V0,
        )
        .unwrap_err();
        assert_eq!(error, LoadError::DecryptionError);
        _remove_if_present(test_path);
    }
}
