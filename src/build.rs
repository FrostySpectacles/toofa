#[cfg(windows)]
extern crate winres;

#[cfg(windows)]
fn main() {
    winres::WindowsResource::new()
        .set_icon("src/icons/logo.ico")
        .compile()
        .unwrap();
}

#[cfg(not(windows))]
fn main() {}
