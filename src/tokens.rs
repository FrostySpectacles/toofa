use chrono::{DateTime, Utc};
use core::fmt;
use serde::{de::Error, Deserialize, Deserializer, Serialize};
use std::collections::{HashMap, HashSet};
use totp_rs::{TotpUrlError, TOTP};
use uuid::Uuid;

use crate::file_ops::EncryptionKey;

#[derive(Debug, Clone, PartialEq)]
pub struct Credential {
    pub id: String,
    pub totp: TOTP,
    pub created: DateTime<Utc>,
    pub last_modified: DateTime<Utc>,
}

// Auxiliary data structure to assist file I/O
#[derive(Serialize, Deserialize)]
struct CredentialIntermediate {
    id: String,
    totp_url: String,
    #[serde(with = "chrono::serde::ts_nanoseconds")]
    created: DateTime<Utc>,
    #[serde(with = "chrono::serde::ts_nanoseconds")]
    last_modified: DateTime<Utc>,
}

impl<'de> Deserialize<'de> for Credential {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let intermediate = CredentialIntermediate::deserialize(deserializer)?;
        let totp = TOTP::from_url_unchecked(intermediate.totp_url)
            .map_err(|_| Error::custom("Unparseable credential"))?;
        Ok(Credential {
            id: intermediate.id,
            totp,
            created: intermediate.created,
            last_modified: intermediate.last_modified,
        })
    }
}

impl Serialize for Credential {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let intermediate = CredentialIntermediate {
            id: self.id.clone(),
            totp_url: self.totp.get_url(),
            created: self.created,
            last_modified: self.last_modified,
        };
        intermediate.serialize(serializer)
    }
}

#[derive(Debug)]
pub struct CodeGenerationFailure;

impl fmt::Display for Credential {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let issuer = self.totp.issuer.clone().unwrap_or_default();
        match issuer.is_empty() {
            true => write!(f, "{}", self.totp.account_name),
            false => write!(f, "{}: {}", issuer, self.totp.account_name),
        }
    }
}

impl Credential {
    pub fn from_totp(totp: TOTP) -> Self {
        let created_time = Utc::now();

        Credential {
            id: Uuid::new_v4().to_string(),
            totp,
            created: created_time,
            last_modified: created_time,
        }
    }

    pub fn generate_token(&self) -> Result<String, CodeGenerationFailure> {
        self.totp
            .generate_current()
            .map_err(|_| CodeGenerationFailure)
    }
}

#[derive(Debug, Clone)]
pub struct CredentialStore {
    pub encryption_key: Option<EncryptionKey>,
    pub credentials: Vec<Credential>,
    pub deleted_credential_ids: HashSet<String>,
    pub last_modified: DateTime<Utc>,
}

#[derive(Debug, PartialEq)]
pub enum CredentialCreationError {
    GeneralFailure,
    WeakCredential(Credential),
}

pub struct InferredCredentialList {
    pub regular_credentials: Vec<Credential>,
    pub weak_credentials: Vec<Credential>,
}

pub struct MergeResult {
    pub credential_store: CredentialStore,
    pub deleted_credentials: Vec<Credential>,
}

impl CredentialStore {
    pub fn new(encryption_key: Option<EncryptionKey>) -> CredentialStore {
        CredentialStore {
            encryption_key,
            credentials: vec![],
            deleted_credential_ids: HashSet::<String>::new(),
            last_modified: Utc::now(),
        }
    }

    pub fn load(
        encryption_key: Option<EncryptionKey>,
        credentials: Vec<Credential>,
        deleted_credential_ids: HashSet<String>,
        last_modified: DateTime<Utc>,
    ) -> CredentialStore {
        CredentialStore {
            encryption_key,
            credentials,
            deleted_credential_ids,
            last_modified,
        }
    }

    pub fn infer_credentials_from(&self, input: &str) -> InferredCredentialList {
        // Be chill about users accidentally copying excess whitespace
        let trimmed_input = input.trim();

        let mut str_entries: Vec<&str> = trimmed_input.lines().collect();
        str_entries.retain(|item| !item.is_empty());

        let mut regular_credentials = vec![];
        let mut weak_credentials = vec![];
        for str_entry in str_entries {
            match self.create_credential_for(str_entry) {
                Ok(credential) => {
                    regular_credentials.push(credential);
                }
                Err(CredentialCreationError::WeakCredential(credential)) => {
                    weak_credentials.push(credential);
                }
                Err(..) => {}
            };
        }

        InferredCredentialList {
            regular_credentials,
            weak_credentials,
        }
    }

    pub fn create_credential_for(
        &self,
        otpauth_url: &str,
    ) -> Result<Credential, CredentialCreationError> {
        let secure_totp_result = TOTP::from_url(otpauth_url);

        match secure_totp_result {
            Ok(totp) => Ok(Credential::from_totp(totp)),
            Err(TotpUrlError::SecretSize(..)) => {
                match self.create_unchecked_credential_for(otpauth_url) {
                    Ok(credential) => Err(CredentialCreationError::WeakCredential(credential)),
                    Err(error) => Err(error),
                }
            }
            _ => Err(CredentialCreationError::GeneralFailure),
        }
    }

    pub fn create_unchecked_credential_for(
        &self,
        otpauth_url: &str,
    ) -> Result<Credential, CredentialCreationError> {
        match TOTP::from_url_unchecked(otpauth_url) {
            Ok(insecure_totp) => Ok(Credential::from_totp(insecure_totp)),
            _ => Err(CredentialCreationError::GeneralFailure),
        }
    }

    pub fn add_credential(&mut self, credential: Credential) {
        self.credentials.push(credential);
    }

    pub fn add_from_url(
        &mut self,
        otpauth_url: &str,
    ) -> Result<Credential, CredentialCreationError> {
        let credential = self.create_credential_for(otpauth_url)?;
        self.add_credential(credential.clone());
        self.last_modified = credential.last_modified;
        Ok(credential)
    }

    pub fn rename_credential(
        &mut self,
        target: Credential,
        new_issuer: Option<String>,
        new_account_name: String,
    ) -> Result<Credential, ()> {
        let current_time = Utc::now();
        let old_length = self.credentials.len();
        self.credentials
            .retain(|credential| credential.id != target.id);

        if old_length == self.credentials.len() {
            // Nothing got replaced
            return Err(());
        }

        let new_totp = TOTP::new_unchecked(
            target.totp.algorithm,
            target.totp.digits,
            target.totp.skew,
            target.totp.step,
            target.totp.secret,
            new_issuer,
            new_account_name,
        );
        let new_credential = Credential {
            id: target.id,
            totp: new_totp,
            created: target.created,
            last_modified: current_time,
        };
        self.credentials.push(new_credential.clone());
        self.last_modified = current_time;
        Ok(new_credential.clone())
    }

    pub fn search_by_name(&self, query: &str) -> Vec<Credential> {
        self.credentials
            .iter()
            .filter(|credential| credential.totp.account_name.contains(query))
            .cloned()
            .collect()
    }

    pub fn all_credentials(&self) -> Vec<Credential> {
        self.search_by_name("")
    }

    pub fn remove_credential(&mut self, target: Credential) {
        let old_count = self.credentials.len();
        self.credentials
            .retain(|credential| credential.id != target.id);

        if old_count != self.credentials.len() {
            self.last_modified = Utc::now();
            self.deleted_credential_ids.insert(target.id);
        }
    }

    pub fn merge(mine: &CredentialStore, theirs: &CredentialStore) -> MergeResult {
        let mut deletion_union = mine.deleted_credential_ids.clone();
        deletion_union.extend(theirs.deleted_credential_ids.clone());

        let mut zipper = HashMap::new();
        for credential in mine.all_credentials() {
            zipper
                .entry(credential.id.clone())
                .or_insert(Vec::new())
                .push(credential.clone());
        }
        for credential in theirs.all_credentials() {
            zipper
                .entry(credential.id.clone())
                .or_insert(Vec::new())
                .push(credential.clone());
        }

        let mut merged_credentials = Vec::<Credential>::new();
        for (key, value) in zipper.iter() {
            if !deletion_union.contains(key) {
                match value.into_iter().reduce(|a, b| {
                    if a.last_modified < b.last_modified {
                        return b;
                    }
                    return a;
                }) {
                    Some(item_to_add) => {
                        merged_credentials.push(item_to_add.clone());
                    }
                    None => {}
                }
            }
        }

        // Sort to make results more predictable
        merged_credentials.sort_by(|a, b| a.id.cmp(&b.id));

        let deleted_credentials: Vec<_> = mine
            .credentials
            .clone()
            .into_iter()
            .filter(|x| deletion_union.contains(&x.id))
            .collect();

        MergeResult {
            credential_store: CredentialStore::load(
                mine.encryption_key.clone(),
                merged_credentials,
                deletion_union,
                mine.last_modified.max(theirs.last_modified),
            ),
            deleted_credentials,
        }
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;
    use std::{collections::HashSet, time::Duration};

    use chrono::DateTime;
    use totp_rs::TOTP;

    use crate::tokens::CredentialCreationError;

    use super::{Credential, CredentialStore};

    #[test]
    fn test_fill_last_modified_when_instantiating() {
        let cred_store = CredentialStore::new(None);
        assert!(
            DateTime::parse_from_rfc3339("2023-01-01T00:00:00Z").unwrap()
                < cred_store.last_modified
        );
    }

    #[test]
    fn test_initialize_other_fields() {
        let cred_store = CredentialStore::new(None);
        assert_eq!(cred_store.deleted_credential_ids, HashSet::<String>::new());
    }

    #[test]
    fn test_add_search_generate() {
        let totp_url = "otpauth://totp/Foo?secret=xy6uz4tcs3mk5bxemykmtatvd7i7vndcekgpb2j2ushbko23bvashifp&digits=7&period=10";
        let totp = TOTP::from_url(totp_url).unwrap();
        let mut cred_store = CredentialStore::new(None);
        let original_last_modified = cred_store.last_modified;

        std::thread::sleep(Duration::from_nanos(1));
        cred_store.add_from_url(totp_url).unwrap();
        assert!(original_last_modified < cred_store.last_modified);
        assert_eq!(cred_store.deleted_credential_ids, HashSet::<String>::new());

        let search_results = cred_store.search_by_name("Foo");

        assert_eq!(
            vec![totp],
            search_results
                .iter()
                .map(|x| x.totp.clone())
                .collect::<Vec<_>>(),
        );
        assert_eq!(search_results[0].created, search_results[0].last_modified);

        let generated_token = search_results[0].generate_token().unwrap();
        assert!(!generated_token.is_empty());
    }

    #[test]
    fn test_create_credential() {
        let totp_url = "otpauth://totp/Foo?secret=xy6uz4tcs3mk5bxemykmtatvd7i7vndcekgpb2j2ushbko23bvashifp&digits=7&period=10";
        let expected_totp = TOTP::from_url(totp_url).unwrap();
        let cred_store = CredentialStore::new(None);
        let actual_credential = cred_store.create_credential_for(totp_url).unwrap();
        assert_eq!(actual_credential.created, actual_credential.last_modified);

        let search_results = cred_store.search_by_name("");
        assert_eq!(search_results, vec![]);

        assert_eq!(expected_totp, actual_credential.totp);
        assert!(actual_credential.id.len() > 5);

        assert_eq!(cred_store.deleted_credential_ids, HashSet::<String>::new());
    }

    #[test]
    fn test_allow_create_credential_inferior_security() {
        let totp_url = "otpauth://totp/Test:Foo?secret=ksex24e23vsjg7t2&algorithm=SHA256&digits=8&period=30&lock=false";
        let expected_totp = TOTP::from_url_unchecked(totp_url).unwrap();
        let cred_store = CredentialStore::new(None);
        match cred_store.create_credential_for(totp_url) {
            Err(CredentialCreationError::WeakCredential(weak_cred))
                if weak_cred.totp == expected_totp => {}
            _ => assert!(false),
        }
    }

    #[test]
    fn test_create_unchecked_credential() {
        let totp_url = "otpauth://totp/Test:Foo?secret=ksex24e23vsjg7t2&algorithm=SHA256&digits=8&period=30&lock=false";
        let expected_totp = TOTP::from_url_unchecked(totp_url).unwrap();
        let cred_store = CredentialStore::new(None);
        let credential = cred_store
            .create_unchecked_credential_for(totp_url)
            .unwrap();
        assert_eq!(credential.totp, expected_totp);
        assert_eq!(credential.created, credential.last_modified);
    }

    #[test]
    fn test_infer_credentials_from_otp_list() {
        let otp_list_str = "otpauth://totp/Foo?secret=xy6uz4tcs3mk5bxemykmtatvd7i7vndcekgpb2j2ushbko23bvashifp&digits=7&period=10\n       otpauth://totp/Test?secret=ljrqbxytv5z4hk7up3zxmbf2snfist2ilc4cbxyu7o6fwng4p4lbpyah&algorithm=SHA256&digits=6&period=30&lock=false\n otpauth://totp/Test:Foo?secret=ksex24e23vsjg7t2&algorithm=SHA256&digits=8&period=30&lock=false";
        let expected_totp_1 = TOTP::from_url("otpauth://totp/Foo?secret=xy6uz4tcs3mk5bxemykmtatvd7i7vndcekgpb2j2ushbko23bvashifp&digits=7&period=10").unwrap();
        let expected_totp_2 = TOTP::from_url("otpauth://totp/Test?secret=ljrqbxytv5z4hk7up3zxmbf2snfist2ilc4cbxyu7o6fwng4p4lbpyah&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        let expected_unsafe_totp = TOTP::from_url_unchecked("otpauth://totp/Test:Foo?secret=ksex24e23vsjg7t2&algorithm=SHA256&digits=8&period=30&lock=false").unwrap();
        let cred_store = CredentialStore::new(None);
        let actual_inferred_credentials = cred_store.infer_credentials_from(otp_list_str);
        let actual_regular_credentials = actual_inferred_credentials.regular_credentials;
        assert_eq!(actual_regular_credentials.len(), 2);
        assert_eq!(actual_inferred_credentials.weak_credentials.len(), 1);

        let search_results = cred_store.search_by_name("");
        assert_eq!(search_results, vec![]);

        assert_eq!(actual_regular_credentials[0].totp, expected_totp_1);
        assert!(actual_regular_credentials[0].id.len() > 5);
        assert_eq!(
            actual_regular_credentials[0].created,
            actual_regular_credentials[0].last_modified
        );

        assert_eq!(actual_regular_credentials[1].totp, expected_totp_2);
        assert!(actual_regular_credentials[1].id.len() > 5);
        assert_eq!(
            actual_regular_credentials[1].created,
            actual_regular_credentials[1].last_modified
        );

        assert_ne!(
            actual_regular_credentials[0].id,
            actual_regular_credentials[1].id
        );
        assert_eq!(
            actual_inferred_credentials.weak_credentials[0].totp,
            expected_unsafe_totp,
        );
        assert!(actual_inferred_credentials.weak_credentials[0].id.len() > 5);
        assert_eq!(
            actual_inferred_credentials.weak_credentials[0].totp,
            expected_unsafe_totp,
        );

        assert_eq!(cred_store.deleted_credential_ids, HashSet::<String>::new());
    }

    #[test]
    fn test_remove_credential_that_does_exist() {
        let mut cred_store = CredentialStore::new(None);
        let credential_to_survive = cred_store.add_from_url("otpauth://totp/Foo?secret=xy6uz4tcs3mk5bxemykmtatvd7i7vndcekgpb2j2ushbko23bvashifp&digits=7&period=1").unwrap();
        let credential_to_remove = cred_store.add_from_url("otpauth://totp/Test?secret=ljrqbxytv5z4hk7up3zxmbf2snfist2ilc4cbxyu7o6fwng4p4lbpyah&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        let credential_id_to_remove = credential_to_remove.id.clone();
        let original_last_modified = cred_store.last_modified;

        std::thread::sleep(Duration::from_nanos(1));
        cred_store.remove_credential(credential_to_remove);

        assert!(original_last_modified < cred_store.last_modified);
        assert_eq!(cred_store.all_credentials(), vec![credential_to_survive]);
        assert_eq!(
            cred_store.deleted_credential_ids,
            HashSet::from([credential_id_to_remove])
        );
    }

    #[test]
    fn test_remove_credential_that_does_not_exist() {
        let mut cred_store = CredentialStore::new(None);
        let credential_to_survive = cred_store.add_from_url("otpauth://totp/Foo?secret=xy6uz4tcs3mk5bxemykmtatvd7i7vndcekgpb2j2ushbko23bvashifp&digits=7&period=1").unwrap();
        let credential_to_remove = cred_store.create_credential_for("otpauth://totp/Test?secret=ljrqbxytv5z4hk7up3zxmbf2snfist2ilc4cbxyu7o6fwng4p4lbpyah&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        let original_last_modified = cred_store.last_modified;

        std::thread::sleep(Duration::from_nanos(1));
        cred_store.remove_credential(credential_to_remove);

        assert_eq!(original_last_modified, cred_store.last_modified);
        assert_eq!(cred_store.all_credentials(), vec![credential_to_survive]);
        assert_eq!(cred_store.deleted_credential_ids, HashSet::<String>::new());
    }

    #[test]
    fn test_rename_credential_that_exists() {
        let mut cred_store = CredentialStore::new(None);
        let credential_to_survive = cred_store.add_from_url("otpauth://totp/Foo?secret=xy6uz4tcs3mk5bxemykmtatvd7i7vndcekgpb2j2ushbko23bvashifp&digits=7&period=1").unwrap();
        let credential_to_rename = cred_store.add_from_url("otpauth://totp/Test?secret=ljrqbxytv5z4hk7up3zxmbf2snfist2ilc4cbxyu7o6fwng4p4lbpyah&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        let original_last_modified = cred_store.last_modified;

        std::thread::sleep(Duration::from_nanos(1));
        cred_store
            .rename_credential(
                credential_to_rename.clone(),
                Some(String::from("FooInc")),
                String::from("ActName"),
            )
            .unwrap();
        let renamed_credential_from_store = cred_store.search_by_name("ActName")[0].clone();
        assert!(
            renamed_credential_from_store.created < renamed_credential_from_store.last_modified
        );

        let mut recreated_renamed_cred = cred_store.create_credential_for("otpauth://totp/ActName?secret=ljrqbxytv5z4hk7up3zxmbf2snfist2ilc4cbxyu7o6fwng4p4lbpyah&algorithm=SHA256&digits=6&period=30&issuer=FooInc&lock=false").unwrap();
        recreated_renamed_cred.id = renamed_credential_from_store.id.clone();
        assert!(original_last_modified < cred_store.last_modified);
        assert_eq!(cred_store.deleted_credential_ids, HashSet::<String>::new());
        assert_eq!(cred_store.all_credentials().len(), 2);
        assert_eq!(cred_store.all_credentials()[0], credential_to_survive);
        assert_eq!(
            renamed_credential_from_store.totp,
            recreated_renamed_cred.totp
        );
    }

    #[test]
    fn test_rename_credential_that_does_not_exist() {
        let mut cred_store = CredentialStore::new(None);
        let credential_to_rename = cred_store.create_credential_for("otpauth://totp/Test?secret=ljrqbxytv5z4hk7up3zxmbf2snfist2ilc4cbxyu7o6fwng4p4lbpyah&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        let original_last_modified = cred_store.last_modified;

        std::thread::sleep(Duration::from_nanos(1));
        let err = cred_store
            .rename_credential(
                credential_to_rename.clone(),
                Some(String::from("FooInc")),
                String::from("ActName"),
            )
            .unwrap_err();
        assert_eq!(err, ());
        assert_eq!(cred_store.all_credentials().len(), 0);
        assert_eq!(original_last_modified, cred_store.last_modified);
    }

    fn _verify_twoway_merge(
        cred_store_mine: CredentialStore,
        cred_store_theirs: CredentialStore,
        merge_to_expect: CredentialStore,
        deleted_credentials_to_expect: Vec<Credential>,
    ) {
        let merge_result = CredentialStore::merge(&cred_store_mine, &cred_store_theirs);

        let sorted_expected_credentials = merge_to_expect
            .credentials
            .clone()
            .sort_by(|a, b| a.id.cmp(&b.id));
        let sorted_actual_credentials = merge_result
            .credential_store
            .credentials
            .clone()
            .sort_by(|a, b| a.id.cmp(&b.id));

        assert_eq!(sorted_expected_credentials, sorted_actual_credentials);
        assert_eq!(
            merge_result.credential_store.last_modified,
            cred_store_mine
                .last_modified
                .max(cred_store_theirs.last_modified)
        );
        assert_eq!(
            deleted_credentials_to_expect,
            merge_result.deleted_credentials
        );

        let remerged_result =
            CredentialStore::merge(&merge_result.credential_store, &cred_store_theirs);
        assert_eq!(
            merge_result.credential_store.credentials,
            remerged_result.credential_store.credentials
        );
        assert_eq!(
            merge_result.credential_store.last_modified,
            remerged_result.credential_store.last_modified
        );
    }

    #[test]
    fn test_merge_two_clients_add() {
        let mut cred_store_left = CredentialStore::new(None);
        let credential_left = cred_store_left.create_credential_for("otpauth://totp/Left?secret=3abxdcs56tpkfycojpomde3ztouqrbx5hcgnzwf5xte4iahc336dg3rb&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        cred_store_left.add_credential(credential_left.clone());

        let mut cred_store_right = CredentialStore::new(None);
        let credential_right = cred_store_right.create_credential_for("otpauth://totp/Right?secret=cg3kh2zb72ckjwlihypk2sqcovmfwt4cmnd4kvxlvkonlxunjcegsl5g&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        cred_store_right.add_credential(credential_right.clone());

        let mut merge_to_expect = CredentialStore::new(None);
        merge_to_expect.add_credential(credential_left.clone());
        merge_to_expect.add_credential(credential_right.clone());

        _verify_twoway_merge(
            cred_store_left.clone(),
            cred_store_right.clone(),
            merge_to_expect.clone(),
            vec![],
        );
        _verify_twoway_merge(
            cred_store_right.clone(),
            cred_store_left.clone(),
            merge_to_expect.clone(),
            vec![],
        );
    }

    #[test]
    fn test_merge_two_clients_rename() {
        let mut cred_store_base = CredentialStore::new(None);
        let cred_no_rename = cred_store_base.create_credential_for("otpauth://totp/Base?secret=7qgzkik3iqhllz2etflqaihe2gzay2tlqztqm57jgbc2vorc3pkvtgie&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        let cred_to_rename = cred_store_base.create_credential_for("otpauth://totp/Base?secret=qctircax2uspeanxyfba7hibbxoankhli5dlvxeflkowx2iagytl5y45&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        cred_store_base.add_credential(cred_no_rename.clone());
        cred_store_base.add_credential(cred_to_rename.clone());

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_left = cred_store_base.clone();
        cred_store_left
            .rename_credential(cred_to_rename.clone(), None, String::from("Rn1"))
            .unwrap();

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_right = cred_store_base.clone();
        cred_store_right
            .rename_credential(cred_to_rename.clone(), None, String::from("Rn2"))
            .unwrap();

        _verify_twoway_merge(
            cred_store_left.clone(),
            cred_store_right.clone(),
            cred_store_right.clone(),
            vec![],
        );
        _verify_twoway_merge(
            cred_store_right.clone(),
            cred_store_left.clone(),
            cred_store_right.clone(),
            vec![],
        );
    }

    #[test]
    fn test_merge_two_clients_remove() {
        let mut cred_store_left = CredentialStore::new(None);
        let credential_to_keep = cred_store_left.create_credential_for("otpauth://totp/Keep?secret=ljrqbxytv5z4hk7up3zxmbf2snfist2ilc4cbxyu7o6fwng4p4lbpyah&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        let credential_to_remove = cred_store_left.create_credential_for("otpauth://totp/Remove?secret=ljrqbxytv5z4hk7up3zxmbf2snfist2ilc4cbxyu7o6fwng4p4lbpyah&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        cred_store_left.add_credential(credential_to_keep.clone());
        cred_store_left.add_credential(credential_to_remove.clone());

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_right = cred_store_left.clone();
        cred_store_right.remove_credential(credential_to_remove.clone());

        let mut merge_to_expect = CredentialStore::new(None);
        merge_to_expect.add_credential(credential_to_keep.clone());

        _verify_twoway_merge(
            cred_store_left.clone(),
            cred_store_right.clone(),
            merge_to_expect.clone(),
            vec![credential_to_remove],
        );
        _verify_twoway_merge(
            cred_store_right.clone(),
            cred_store_left.clone(),
            merge_to_expect.clone(),
            vec![],
        );
    }

    fn _verify_threeway_merge(
        cred_store_left: CredentialStore,
        cred_store_mid: CredentialStore,
        cred_store_right: CredentialStore,
        merge_to_expect: CredentialStore,
    ) {
        fn _verify_chain_merge(
            first: &CredentialStore,
            second: &CredentialStore,
            third: &CredentialStore,
            merge_to_expect: &CredentialStore,
        ) {
            let merge_result_1 = CredentialStore::merge(&first, &second);
            let merge_result_2 = CredentialStore::merge(&merge_result_1.credential_store, &third);

            let mut sorted_expected_credentials = merge_to_expect.credentials.clone();
            let mut sorted_actual_credentials = merge_result_2.credential_store.credentials.clone();
            sorted_expected_credentials.sort_by(|a, b| a.id.cmp(&b.id));
            sorted_actual_credentials.sort_by(|a, b| a.id.cmp(&b.id));

            assert_eq!(sorted_expected_credentials, sorted_actual_credentials);
            assert_eq!(
                first
                    .last_modified
                    .max(second.last_modified.max(third.last_modified)),
                merge_result_2.credential_store.last_modified
            );

            // Idempotency test
            let remerged_result_1 =
                CredentialStore::merge(&merge_result_2.credential_store, &first);
            let remerged_result_2 =
                CredentialStore::merge(&remerged_result_1.credential_store, &second);
            let remerged_result_3 =
                CredentialStore::merge(&remerged_result_2.credential_store, &third);
            assert_eq!(
                merge_result_2.credential_store.credentials,
                remerged_result_3.credential_store.credentials
            );
            assert_eq!(
                merge_result_2.credential_store.last_modified,
                remerged_result_3.credential_store.last_modified
            );
        }

        _verify_chain_merge(
            &cred_store_left,
            &cred_store_mid,
            &cred_store_right,
            &merge_to_expect,
        );
        _verify_chain_merge(
            &cred_store_mid,
            &cred_store_left,
            &cred_store_right,
            &merge_to_expect,
        );
        _verify_chain_merge(
            &cred_store_right,
            &cred_store_mid,
            &cred_store_left,
            &merge_to_expect,
        );
        _verify_chain_merge(
            &cred_store_mid,
            &cred_store_right,
            &cred_store_left,
            &merge_to_expect,
        );
        _verify_chain_merge(
            &cred_store_right,
            &cred_store_left,
            &cred_store_mid,
            &merge_to_expect,
        );
    }

    #[test]
    fn test_merge_three_clients_add_everywhere() {
        let mut cred_store_left = CredentialStore::new(None);
        let credential_to_add_1 = cred_store_left.create_credential_for("otpauth://totp/One?secret=7qgzkik3iqhllz2etflqaihe2gzay2tlqztqm57jgbc2vorc3pkvtgie&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        cred_store_left.add_credential(credential_to_add_1.clone());

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_mid = CredentialStore::new(None);
        let credential_to_add_2 = cred_store_mid.create_credential_for("otpauth://totp/Two?secret=ljrqbxytv5z4hk7up3zxmbf2snfist2ilc4cbxyu7o6fwng4p4lbpyah&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        cred_store_mid.add_credential(credential_to_add_2.clone());

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_right = CredentialStore::new(None);
        let credential_to_add_3 = cred_store_right.create_credential_for("otpauth://totp/Three?secret=a7hc35nrbgtevzwlpmgdlwsef4qetlyutjl5vbhhfw2k3s2rbzj5u22e&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        cred_store_right.add_credential(credential_to_add_3.clone());

        let mut merge_to_expect = CredentialStore::new(None);
        merge_to_expect.add_credential(credential_to_add_1.clone());
        merge_to_expect.add_credential(credential_to_add_2.clone());
        merge_to_expect.add_credential(credential_to_add_3.clone());
        _verify_threeway_merge(
            cred_store_left,
            cred_store_mid,
            cred_store_right,
            merge_to_expect,
        );
    }

    #[test]
    fn test_merge_three_clients_rename() {
        let mut cred_store_base = CredentialStore::new(None);
        let base_credential = cred_store_base.create_credential_for("otpauth://totp/Base?secret=7qgzkik3iqhllz2etflqaihe2gzay2tlqztqm57jgbc2vorc3pkvtgie&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        cred_store_base.add_credential(base_credential.clone());

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_left = cred_store_base.clone();
        cred_store_left
            .rename_credential(base_credential.clone(), None, String::from("Rn1"))
            .unwrap();

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_mid = cred_store_base.clone();
        cred_store_mid
            .rename_credential(base_credential.clone(), None, String::from("Rn2"))
            .unwrap();

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_right = cred_store_base.clone();
        cred_store_right
            .rename_credential(base_credential.clone(), None, String::from("Rn3"))
            .unwrap();

        _verify_threeway_merge(
            cred_store_left,
            cred_store_mid,
            cred_store_right.clone(),
            cred_store_right.clone(),
        );
    }

    #[test]
    fn test_merge_three_clients_remove() {
        let mut cred_store_base = CredentialStore::new(None);
        let base_credential_1 = cred_store_base.create_credential_for("otpauth://totp/Base1?secret=a7hc35nrbgtevzwlpmgdlwsef4qetlyutjl5vbhhfw2k3s2rbzj5u22e&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        let base_credential_2 = cred_store_base.create_credential_for("otpauth://totp/Base2?secret=7gli73dl2f7dmsm2eifalyuqv4dibgx5pydb3xlujpfmoxkpwshy4qxg&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        let base_credential_3 = cred_store_base.create_credential_for("otpauth://totp/Base3?secret=pajnmevaiyipzjqsgzqnme3x4gpc4mqmtdz52n7rnza57lhzeckbgf5g&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        cred_store_base.add_credential(base_credential_1.clone());
        cred_store_base.add_credential(base_credential_2.clone());
        cred_store_base.add_credential(base_credential_3.clone());

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_left = cred_store_base.clone();
        cred_store_left.remove_credential(base_credential_1);

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_mid = cred_store_base.clone();
        cred_store_mid.remove_credential(base_credential_2);

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_right = cred_store_base.clone();
        cred_store_right.remove_credential(base_credential_3);

        let merge_to_expect = CredentialStore::new(None);
        _verify_threeway_merge(
            cred_store_left,
            cred_store_mid,
            cred_store_right,
            merge_to_expect,
        );
    }

    #[test]
    fn test_merge_three_clients_everything() {
        let mut cred_store_base = CredentialStore::new(None);
        let remove_from_right_credential = cred_store_base.create_credential_for("otpauth://totp/Base1?secret=a7hc35nrbgtevzwlpmgdlwsef4qetlyutjl5vbhhfw2k3s2rbzj5u22e&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        let rename_from_mid_credential = cred_store_base.create_credential_for("otpauth://totp/Base2?secret=7gli73dl2f7dmsm2eifalyuqv4dibgx5pydb3xlujpfmoxkpwshy4qxg&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        cred_store_base.add_credential(remove_from_right_credential.clone());
        cred_store_base.add_credential(rename_from_mid_credential.clone());

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_left = cred_store_base.clone();
        let added_from_left_credential = cred_store_base.create_credential_for("otpauth://totp/Base3?secret=pajnmevaiyipzjqsgzqnme3x4gpc4mqmtdz52n7rnza57lhzeckbgf5g&algorithm=SHA256&digits=6&period=30&lock=false").unwrap();
        cred_store_left.add_credential(added_from_left_credential.clone());

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_mid = cred_store_base.clone();
        let renamed_from_mid_credential = cred_store_mid
            .rename_credential(rename_from_mid_credential, None, String::from("Rn2"))
            .unwrap();

        std::thread::sleep(Duration::from_nanos(1));
        let mut cred_store_right = cred_store_base.clone();
        cred_store_right.remove_credential(remove_from_right_credential.clone());

        let mut merge_to_expect = CredentialStore::new(None);
        merge_to_expect.last_modified = cred_store_right.last_modified.clone();
        merge_to_expect.credentials = vec![renamed_from_mid_credential, added_from_left_credential];
        _verify_threeway_merge(
            cred_store_left,
            cred_store_mid,
            cred_store_right,
            merge_to_expect,
        );
    }
}
