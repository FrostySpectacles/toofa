use arboard::Clipboard;
use bardecoder::default_decoder;
use file_ops::{
    get_encrypted_cred_store_from_config, sniff_config_file_type, ConfigFileType, LoadError,
};
use image::{DynamicImage, ImageBuffer, Rgba};
use inquire::{Confirm, Password, Select, Text};
use rand::RngCore;
use thiserror::Error;
mod file_ops;
mod tokens;

use std::{
    env,
    io::{self, Write},
    path::{Path, PathBuf},
    process,
};

use crate::{
    file_ops::{get_unencrypted_cred_store_from_config, save_cred_store_to_config, EncryptionKey},
    tokens::{Credential, CredentialCreationError, CredentialStore},
};
use aes_gcm::aead::OsRng;
use clap::{Parser, Subcommand, ValueEnum};
use dirs::config_dir;

fn get_config_file_path() -> Result<PathBuf, CliError> {
    match config_dir() {
        Some(config_dir) => Ok(config_dir.join("toofa").join("toofa.json")),
        None => Err(CliError::CannotFindConfig),
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
enum ExportType {
    Plain,
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Subcommand)]
enum BaseCommand {
    #[command(about = "Add a new account by its otpauth:// URL")]
    Add,
    #[command(about = "Generate a code for an account")]
    Get,
    #[command(about = "Edit the label of an account")]
    Edit,
    #[command(about = "Migrate/backup account to another application")]
    Export { export_type: ExportType },
    #[command(about = "Create a new credential store")]
    Init,
    #[command(about = "Merge another credential store with yours")]
    Merge { other_config_path: String },
    #[command(about = "Change password of the credential store")]
    Passwd,
    #[command(about = "Bulk import QR codes and otpauth URLs from system clipboard")]
    Paste,
    #[command(about = "Permanently remove an account from the credential store")]
    Remove,
}

#[derive(Parser)]
struct Cli {
    #[clap(subcommand)]
    base_command: BaseCommand,
}

#[derive(Error, Debug)]
pub enum CliError {
    #[error("No existing credential store found. Initiate a new one.")]
    NoCredentialStore,
    #[error("Could not open your credential store. Your password may be incorrect.")]
    DecryptionError,
    #[error("Cannot find out where the configuration file is stored.")]
    CannotFindConfig,
    #[error("Could not open your credential store. It may be corrupt or intended for a newer version of Toofa.")]
    CannotLoadCredentialStore,
    #[error("A configuration file already exists at {0}. If you want to start fresh, throw away the file first.")]
    WontOverwriteExistingCredentialStore(String),
    #[error("Action canceled.")]
    ActionCanceled,
    #[error("Credential store initialization failed.")]
    InitFailed,
    #[error("{0}")]
    InvalidInput(String),
    #[error("Could not save changes to your credential store.")]
    WriteError,
    #[error("Failed to generate token for {0}.")]
    TokenGenerationFailed(String),
    #[error("Clipboard is not accessible. This may be a permission issue.")]
    ClipboardInaccessible,
    #[error("No QR codes were found in the screenshot on your clipboard.")]
    NoQrsFound,
    #[error(
        "Could not parse input from clipboard. Some tips:\n\
            - Toofa accepts valid otpauth:// links.\n\
            - If you supply multiple accounts at once, only supply one per line."
    )]
    NoValidClipboardInput,
    #[error("No accounts found.")]
    NoAccountsFound,
    #[error("Other credential store not found. Verify that the path is correct.")]
    IncorrectCredentialStorePath,
}

fn get_default_cred_store() -> Result<CredentialStore, CliError> {
    let path_buf = get_config_file_path()?;
    let path = path_buf.as_path();
    if !path.exists() {
        return Err(CliError::NoCredentialStore);
    }

    get_cred_store(&path)
}

fn get_cred_store(path: &Path) -> Result<CredentialStore, CliError> {
    let config_file_type =
        sniff_config_file_type(path).expect("Could not detect type of existing config file.");

    let cred_store_result = match config_file_type {
        ConfigFileType::Unencrypted => get_unencrypted_cred_store_from_config(path),
        ConfigFileType::Encrypted(encryption_version) => match env::var("TOOFA_PASSWORD") {
            Ok(password) => {
                get_encrypted_cred_store_from_config(path, &password, &encryption_version)
            }
            Err(..) => {
                let password = Password::new("Enter your master password:")
                    .without_confirmation()
                    .prompt()
                    .map_err(|_| CliError::ActionCanceled)?;
                get_encrypted_cred_store_from_config(path, &password, &encryption_version)
            }
        },
    };

    match cred_store_result {
        Ok(cred_store) => Ok(cred_store),
        Err(load_error) if load_error == LoadError::DecryptionError => {
            Err(CliError::DecryptionError)
        }
        Err(_load_error) => Err(CliError::CannotLoadCredentialStore),
    }
}

fn _enter_new_password() -> Result<String, CliError> {
    let password_input = Password::new("Choose a new master password:")
        .with_custom_confirmation_message("Confirm new master password:")
        .with_custom_confirmation_error_message("The passwords do not match.")
        .prompt();

    match password_input {
        Ok(password) => {
            if password.is_empty() {
                let confirm_input = Confirm::new("If you don't enter a password, your account secrets will be stored in plain text. Any application or person with access to your drive may be able to obtain them.\nAre you absolutely sure about this?")
                    .with_default(false)
                    .prompt();
                match confirm_input {
                    Ok(outcome) if !outcome => {
                        return Err(CliError::ActionCanceled);
                    }
                    _ => {}
                }
            }

            return Ok(password);
        }
        Err(_) => Err(CliError::InvalidInput("Invalid input.".to_string())),
    }
}

fn init_cred_store() -> Result<(), CliError> {
    let path_buf = get_config_file_path()?;
    let path = path_buf.as_path();
    if path.exists() {
        return Err(CliError::WontOverwriteExistingCredentialStore(
            path.display().to_string(),
        ));
    }

    let password = _enter_new_password()?;
    match password.as_str() {
        "" => {
            println!("Initializing insecure credential store...");
            let cred_store = CredentialStore::new(None);
            save_cred_store_to_config(path, &cred_store).map_err(|_| CliError::InitFailed)
        }
        _ => {
            println!("Initializing encrypted credential store...");

            let derivation_salt = _make_derivation_salt();
            let encryption_key = EncryptionKey::from_password(&password, derivation_salt);
            let cred_store = CredentialStore::new(Some(encryption_key));
            save_cred_store_to_config(path, &cred_store).map_err(|_| CliError::InitFailed)
        }
    }
}

fn export_cred_store(
    cred_store: &CredentialStore,
    export_type: &ExportType,
) -> Result<(), CliError> {
    match export_type {
        ExportType::Plain => {
            for credential in cred_store.credentials.iter() {
                println!("{}", credential.totp.get_url());
            }
        }
    }
    Ok(())
}

fn merge_cred_stores(
    cred_store: &CredentialStore,
    other_config_path: String,
) -> Result<(), CliError> {
    let other_config_path_obj = Path::new(other_config_path.as_str());
    if !other_config_path_obj.exists() {
        return Err(CliError::IncorrectCredentialStorePath);
    }

    println!("Opening external credential store...");
    let external_cred_store = get_cred_store(&other_config_path_obj)?;
    println!("");

    let merge_result = CredentialStore::merge(&cred_store, &external_cred_store);
    if merge_result.deleted_credentials.len() > 0 {
        println!(
            "The following {} credentials will be permanently deleted:",
            merge_result.deleted_credentials.len()
        );
        for credential in merge_result.deleted_credentials.into_iter() {
            println!("⚠️ {}", credential);
        }
        println!("");
    }
    println!(
        "You had {} credentials. After merge, you will have {}.",
        cred_store.credentials.len(),
        &merge_result.credential_store.credentials.len(),
    );

    let confirm_result = Confirm::new("Save changes?").with_default(false).prompt();
    match confirm_result {
        Ok(answer) if answer => {
            let path_buf = get_config_file_path()?;
            save_cred_store_to_config(path_buf.as_path(), &merge_result.credential_store)
                .map_err(|_| CliError::WriteError)
        }
        _ => Err(CliError::ActionCanceled),
    }
}

fn _make_derivation_salt() -> [u8; 32] {
    let mut derivation_salt = [0u8; 32];
    OsRng.fill_bytes(&mut derivation_salt);
    derivation_salt
}

fn _extract_otpauth_urls() -> Result<String, CliError> {
    let mut clipboard = Clipboard::new().map_err(|_| CliError::ClipboardInaccessible)?;
    if let Ok(clipboard_img) = clipboard.get_image() {
        let width = u32::try_from(clipboard_img.width).unwrap();
        let height = u32::try_from(clipboard_img.height).unwrap();

        let buffer = ImageBuffer::<Rgba<u8>, _>::from_raw(
            width,
            height,
            Vec::<u8>::from(clipboard_img.bytes),
        )
        .unwrap();
        let dyn_image = DynamicImage::ImageRgba8(buffer);

        let decoder = default_decoder();
        let decoded_items = decoder.decode(&dyn_image);
        let mut possible_otpauth_urls = String::from("");
        for decoded_item in decoded_items {
            if let Ok(qr_value) = decoded_item {
                // CredentialStore currently expects one string but checks for multiple values
                possible_otpauth_urls += qr_value.as_str();
                possible_otpauth_urls += "\n";
            }
        }
        if possible_otpauth_urls.len() > 0 {
            return Ok(possible_otpauth_urls);
        } else {
            return Err(CliError::NoQrsFound);
        }
    }

    let clipboard_text = clipboard
        .get_text()
        .map_err(|_| CliError::ClipboardInaccessible)?;

    // People sometimes copy excess whitespace by accident
    return Ok(String::from(clipboard_text.trim()));
}

fn paste_input(cred_store: &mut CredentialStore) -> Result<(), CliError> {
    let clipboard_value = _extract_otpauth_urls()?;
    let inferred_credentials = cred_store.infer_credentials_from(&clipboard_value);

    if inferred_credentials.regular_credentials.len() == 0
        && inferred_credentials.weak_credentials.len() == 0
    {
        return Err(CliError::NoValidClipboardInput);
    }

    if inferred_credentials.regular_credentials.len() > 0 {
        println!("The following accounts were detected:");
        for credential in inferred_credentials.regular_credentials.iter() {
            println!("- {}", credential);
        }

        let confirm_result = Confirm::new(
            format!(
                "Proceed adding {} accounts?",
                &inferred_credentials.regular_credentials.len()
            )
            .as_str(),
        )
        .with_default(true)
        .prompt();

        match confirm_result {
            Ok(answer) if answer => {
                for credential in inferred_credentials.regular_credentials {
                    cred_store.add_credential(credential);
                }
            }
            _ => {}
        }
    }

    if inferred_credentials.weak_credentials.len() > 0 {
        println!(
            "The following accounts were detected but don't meet the security recommendations:"
        );
        for credential in inferred_credentials.weak_credentials.iter() {
            println!("- {}", credential);
        }

        let confirm_result = Confirm::new(
            format!(
                "Proceed adding {} accounts with weaker security?",
                &inferred_credentials.weak_credentials.len()
            )
            .as_str(),
        )
        .with_default(true)
        .prompt();

        match confirm_result {
            Ok(answer) if answer => {
                for credential in inferred_credentials.weak_credentials {
                    cred_store.add_credential(credential);
                }
            }
            _ => {}
        }
    }

    let path_buf = get_config_file_path()?;
    save_cred_store_to_config(path_buf.as_path(), cred_store).map_err(|_| CliError::WriteError)
}

fn add_credential(cred_store: &mut CredentialStore) -> Result<(), CliError> {
    print!("Enter otpauth URL: ");
    io::stdout().flush().expect("I/O flush failed");

    let mut buffer = String::new();
    io::stdin()
        .read_line(&mut buffer)
        .expect("Cannot read line");

    let credential = match cred_store.add_from_url(buffer.as_str()) {
        Err(CredentialCreationError::GeneralFailure) => Err(CliError::InvalidInput(
            "That account secret appears to be invalid.".to_string(),
        )),
        Err(CredentialCreationError::WeakCredential(weak_credential)) => {
            match Confirm::new("This account secret does not meet security recommendations. Secrets are advised to be at least 128 bits in length. Proceed anyway?").with_default(true).prompt() {
                Ok(result) if result => {
                    cred_store.add_credential(weak_credential.clone());
                    Ok(weak_credential)
                },
                _ => Err(CliError::ActionCanceled),
            }
        },
        Ok(credential) => Ok(credential),
    }?;
    println!("Saving account {}", credential.totp.account_name);

    let path_buf = get_config_file_path()?;
    save_cred_store_to_config(path_buf.as_path(), cred_store).map_err(|_| CliError::WriteError)
}

fn _pick_credential(cred_store: &CredentialStore) -> Result<Credential, CliError> {
    // Since inquire implements interactive filtering, we'll rely on that as the search mechanism
    // for now.
    let mut search_results = cred_store.all_credentials();
    search_results.sort_by(|a, b| {
        a.to_string()
            .to_lowercase()
            .cmp(&b.to_string().to_lowercase())
    });

    match search_results.len() {
        1 => Ok(search_results[0].clone()),
        0 => Err(CliError::NoAccountsFound),
        _ => {
            let cred_selection_result =
                Select::new("Select the desired account:", search_results).prompt();
            match cred_selection_result {
                Ok(selected_credential) => Ok(selected_credential),
                Err(..) => Err(CliError::ActionCanceled),
            }
        }
    }
}

fn get_credential(cred_store: &CredentialStore) -> Result<(), CliError> {
    let picked_cred = _pick_credential(&cred_store)?;

    let account_name = picked_cred.totp.account_name.clone();
    let generated_token = picked_cred
        .generate_token()
        .map_err(|_| CliError::TokenGenerationFailed(account_name.to_string()))?;
    println!("{}", generated_token);

    let mut clipboard = Clipboard::new().map_err(|_| CliError::ClipboardInaccessible)?;
    clipboard
        .set_text(generated_token)
        .map_err(|_| CliError::ClipboardInaccessible)?;
    println!("Copied to clipboard.");
    Ok(())
}

fn edit_credential(cred_store: &mut CredentialStore) -> Result<(), CliError> {
    let picked_cred = _pick_credential(&cred_store)?;

    let issuer = picked_cred.totp.issuer.clone().unwrap_or(String::from(""));
    let account_name = picked_cred.totp.account_name.clone();

    let new_issuer_prompt = Text::new("Website (optional):")
        .with_initial_value(&issuer)
        .prompt();
    let new_issuer_str = new_issuer_prompt.map_err(|_| CliError::ActionCanceled)?;
    let new_issuer = Some(new_issuer_str).filter(|s| !s.is_empty());

    let new_account_name_prompt = Text::new("Account name:")
        .with_initial_value(&account_name)
        .prompt();
    let new_account_name = new_account_name_prompt.map_err(|_| CliError::ActionCanceled)?;

    cred_store
        .rename_credential(picked_cred, new_issuer, new_account_name)
        .expect("Rename failed");

    let path_buf = get_config_file_path()?;
    save_cred_store_to_config(path_buf.as_path(), cred_store).map_err(|_| CliError::WriteError)
}

fn remove_credential(cred_store: &mut CredentialStore) -> Result<(), CliError> {
    let picked_credential = _pick_credential(&cred_store)?;

    let confirm_input_result = Confirm::new(
        format!(
            "Permanently delete '{}' from your credential store?",
            picked_credential
        )
        .as_str(),
    )
    .with_default(false)
    .prompt();

    match confirm_input_result {
        Ok(result) if result => {
            cred_store.remove_credential(picked_credential);
            let path_buf = get_config_file_path()?;
            save_cred_store_to_config(path_buf.as_path(), cred_store)
                .map_err(|_| CliError::WriteError)?;
            println!("Account credential has been deleted.");
            Ok(())
        }
        _ => Err(CliError::ActionCanceled),
    }
}

fn change_password(cred_store: &mut CredentialStore) -> Result<(), CliError> {
    let new_password = _enter_new_password()?;
    cred_store.encryption_key = match new_password.as_str() {
        "" => None,
        _ => Some(EncryptionKey::from_password(
            new_password.as_str(),
            _make_derivation_salt(),
        )),
    };
    let path_buf = get_config_file_path()?;
    save_cred_store_to_config(path_buf.as_path(), cred_store).map_err(|_| CliError::WriteError)?;
    println!("Saved configuration with new password.");
    Ok(())
}

fn handle() -> Result<(), CliError> {
    let cli = Cli::parse();
    if cli.base_command == BaseCommand::Init {
        init_cred_store()
    } else {
        let mut cred_store = get_default_cred_store()?;

        match cli.base_command {
            BaseCommand::Add => add_credential(&mut cred_store),
            BaseCommand::Get => get_credential(&cred_store),
            BaseCommand::Edit => edit_credential(&mut cred_store),
            BaseCommand::Export { export_type } => export_cred_store(&cred_store, &export_type),
            BaseCommand::Merge { other_config_path } => {
                merge_cred_stores(&cred_store, other_config_path)
            }
            BaseCommand::Passwd => change_password(&mut cred_store),
            BaseCommand::Paste => paste_input(&mut cred_store),
            BaseCommand::Remove => remove_credential(&mut cred_store),
            BaseCommand::Init => {
                panic!("This is not the right way to initialize the credential store")
            }
        }
    }
}

fn main() {
    match handle() {
        Ok(..) => {}
        Err(error) => {
            println!("{}", error);
            process::exit(1);
        }
    }
}
